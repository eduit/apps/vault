#!/usr/bin/bash

vault secrets enable -path=services -version=2 kv
vault auth enable userpass
vault auth enable ldap
