#!/usr/bin/bash

vault write auth/ldap/config \
      url="ldaps://ldaps-rz-3.ethz.ch,ldaps-rz-2.ethz.ch,ldaps-rz-1.ethz.ch,ldaps-hit-3.ethz.ch,ldaps-hit-2.ethz.ch,ldaps-hit-1.ethz.ch" \
      binddn="cn=eduit_vault_proxy,ou=admins,ou=nethz,ou=id,ou=auth,o=ethz,c=ch" \
      bindpass="$LDAPPW" \
      userattr="uid" \
      userdn="ou=nethz,ou=id,ou=auth,o=ethz,c=ch" \
      groupdn="ou=custom,ou=groups,ou=nethz,ou=id,ou=auth,o=ethz,c=ch" \
      groupfilter="(&(objectClass=posixGroup)(memberUid={{.Username}}))" \
      groupattr="dn" \
      insecure_tls=false
