#!/usr/bin/bash

function check_and_set {
    path=$1
    fields=$2

    vault kv get $path >/dev/null
    if [ "$?" != "0" ]; then
	vault kv put $path $fields
    fi
}


# DP-Portal
# global
check_and_set services/dp-portal/sharepoint-cert 'content=""'
check_and_set services/dp-portal/horizon 'name="" password=""'
check_and_set services/dp-portal/ldap_bind 'name="" password=""'
check_and_set services/dp-portal/sharepoint 'client_id="" thumbprint=""'
check_and_set services/dp-portal/moodle 'token=""'
# dev
for item in dev test prod; do
    check_and_set services/dp-portal/$item/db 'user="" password=""'
done
