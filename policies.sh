#!/usr/bin/bash

vault policy write service-administrator - <<EOF
path "services/*" {
   capabilities = ["create", "list", "read", "update"]
}
EOF

vault policy write service-maintainer - <<EOF
path "services/*" {
   capabilities = ["update"]
}
EOF

vault policy write service-configurator - <<EOF
path "services/*" {
   capabilities = ["read"]
}
EOF

vault write auth/ldap/groups/ID-EDUC-sysadmins-2er policies=service-administrator
