#!/usr/bin/bash

# Remove when certificate is official
export VAULT_SKIP_VERIFY=1

vault login $TOKEN
